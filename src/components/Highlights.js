import React from 'react'

import { Row, Col, Card, Button, Container } from 'react-bootstrap'

export default function Highlights() {

	return (
		<Container fluid>
			<Row className="p-5 center">
				<Col xs={12} md={8}>
					<Card border="light">
						<Card.Body className="cards mx-3">
							<Card.Title className="heading-highlights text-center">About Us</Card.Title>
							<Card.Text>
								We are a Filipino team who works hard to bring joy and happiness by selling a high quality and official products. You will find all BTS related merchandise here. We will always do our best to ship the items to you no matter where you are.

								Thank you for supporting us!
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={8}>
					<Card>
						<Card.Body className="cards mx-3">
							<Card.Title className="heading-highlights text-center">GIVEAWAY ALERT!</Card.Title>
							<Card.Text>
								Here's your chance to win a FREE BTS Album - worth $50. To enter, simply follow and comment on our Instagram account. BONUS if you'd tell us why you want to win. Two winners will be chosen randomly. Entries will be closed on Friday 3/18 at 9PM PHT. Winners will be announced on Saturday 3/19. Goodluck to all and thank you for supporting us!
							</Card.Text>
							<Button variant="info" href="https://www.instagram.com/bts.bighitofficial/?hl=en">Follow Us!</Button>
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}
