import { useContext, useEffect } from 'react'
import { Navigate } from 'react-router-dom'
import UserContext from '../UserContext'
import React from 'react'

export default function Logout() {

    const { unsetUser, setUser } = useContext(UserContext)

    unsetUser();

    useEffect(() => {
        setUser({ id: null })
    }, [])

    // localStorage.clear()

    return (

        <Navigate to="/login" />
    )
}